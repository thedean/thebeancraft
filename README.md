# TheBeanCraft - A Minecraft Server

This repository serves as the main information page and continuous backup solution for the BeanCraft Minecraft Server

## Player Access

This server is whitelisted and player access can be obtained by joining the discord server and requesting access. 

## Modpack information

The modpack used in this server requires the [Technic Launcher](https://www.technicpack.net/download).

The modpack and modpack version: Tekxit 3 (Official) 1.12.2 Version 0.96

There is a possibility of other mod installs that are required. How to do this and the mods that need to be installed is in a separate chat on the BeanCraft Discord.

## Admin Access

To gain admin access on the server and permissions to control/restart the server remotely please make your intent known in the BeanCraft Discord. A current admin will work with you to possibly allow access.

Server administration is performed over [SSH connection](https://wiki.archlinux.org/index.php/Secure_Shell)
To login via SSH Session you will first need to generate and SSH key and have an existing admin install it on the server for you. Help with this process is outside the scope of this document. Once the key is installed type the following command in powershell where USER is the username to login with:

Linux/MacOS (replace [USER] with your username on the server):
```
ssh -i /path/to/key_rsa [USER]@dedotatedwam.org -p 58463
```

Windows (replace [USER] with your username on the server):
```
ssh -i C:\Users\path\to\key_rsa [USER]@dedotatedwam.org -p 58463
```

## How the Instance Works

The server instance runs on Ubuntu 18.04, and is configured to automatically start on boot-up. This is accomplished using [Systemd service files](https://wiki.archlinux.org/index.php/Systemd#Writing_unit_files). In short, there is a file located at `/etc/systemd/system/beancraft.service`. This file does several things:

- Starts the main instance script on boot
- Restarts the instance after 3 minutes if it crashes
- Allows the instance to be managed via Systemd commands
- Gracefully shuts down the instance when rebooting

If the file is modified, new configuration must be loaded using (with sudo):
```
systemctl daemon-reload
```

As stated, this file executes the main instance script. This script, run as user `minecraft`, is located at `/home/minecraft/thebeancraft/start-minecraft-fifo.sh`.
It does several things:

- Creates a special "fifo" pipe in `/tmp/beancraft`
- Makes it so anyone in the "minecraft" group can use that pipe
- Starts the Minecraft instance
- Connects the fifo as a pipe so that commands sent to it are sent to the instance
- Starts a background job to hold the pipe open even when commands aren't being sent

Commands are then sent to the instance using a command, `minectl`.

This command actually invokes a script, placed in `/usr/local/bin`. This script just takes the input from the user and redirects it to the fifo pipe.

## Controlling the Instance

To start, stop, and restart the instance, use systemd **(requires admin access/sudo)**:
```
systemctl start beancraft

systemctl stop beancraft

systemctl restart beancraft
```

To issue commands to the server, use the `minectl` command. **BE SURE TO ENCLOSE THE MINECRAFT COMMAND IN QUOTES**:
```
minectl beancraft "\command goes here"
```
To view the instance log (will scroll automatically as new lines appear), there are two options:
```
minectl beancraft -l

minectl beancraft --log
```
For more help with the `minectl` command:
```
minectl help
```

The Git repository holding some of these minectl related scripts and files can be found at:

[https://gitlab.thorandrew.com/thor/minecraft-management](https://gitlab.thorandrew.com/thor/minecraft-management)

## Basic Minecraft Commands

A tutorial on Minecraft commands is outside the scope of this document. An excellent summary can be found [on the official Minecraft Wiki](https://minecraft.gamepedia.com/Commands#List_and_summary_of_commands).

Commands can be entered either in the Minecraft game console or over SSh as described above. Remember to precede all commands with a forward slash `/`.

## Basic Linux Commands

* **cd** - change directory
```
    cd work
    cd /usr/local/bin
    cd ~
    cd ..
```

* **ls** - list the files and folders of the current directory
```
    ls
```

* **mkdir** - make a new folder, specify the name right after the command
```
    mkdir invoices
    mkdir minecraft
```

* **mv** - move place 1 to place 2 OR rename from name 1 to name 2.
```
    mv player.dat minecraft/player.dat

	mv player.dat player-old.dat
```

* **ping** - ping a specific server to check whether a server or machine is online. Also shows network lag.
```
    ping www.google.com
```

* **ssh** - ssh command to connect to a remote terminal
```
    ssh jack@192.168.4.23
```

